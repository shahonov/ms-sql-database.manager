﻿using Database.Manager.Factories;
using Database.Manager.Interfaces;
using Database.Manager.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace Database.Manager
{
    public static class DependencyInjectionRegistration
    {
        public static void AddDatabaseManagement(this IServiceCollection services, string connectionString)
        {
            services.AddSingleton<IDbHelper, DbHelper>();
            services.AddSingleton<ISqlFactory>(sql => new SqlFactory(connectionString));
            services.AddSingleton<IDbManager, DbManager>();
        }
    }
}
