﻿using System.Data.SqlClient;
using Database.Manager.Interfaces;

namespace Database.Manager.Factories
{
    public class SqlFactory : ISqlFactory
    {
        private readonly string _connectionString;
        public SqlFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection Create()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
