﻿using System.Data.SqlClient;

namespace Database.Manager.Interfaces
{
    public interface ISqlFactory
    {
        SqlConnection Create();
    }
}
