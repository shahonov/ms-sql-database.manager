﻿using System.Data.SqlClient;

namespace Database.Manager.Interfaces
{
    public interface IDbHelper
    {
        string MapToDotNetDataType(string sqlDataType);

        object ParseValue(string value, string dataType);

        string GetColumnInfo(SqlDataReader reader, int index);
    }
}
