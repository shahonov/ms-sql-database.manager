﻿using Database.Manager.Models;
using System.Collections.Generic;

namespace Database.Manager.Interfaces
{
    public interface IDbManager
    {
        IEnumerable<string> GetRaw(string commandText);

        DatabaseResponse ExecuteVoid(string commandText);

        IEnumerable<T> Get<T>(string selectQuery) where T : class;
    }
}
