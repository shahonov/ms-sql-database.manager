﻿using System;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Database.Manager.Models;
using System.Collections.Generic;
using Database.Manager.Interfaces;

namespace Database.Manager.Implementations
{
    public class DbManager : IDbManager
    {
        private readonly ISqlFactory _connection;
        private readonly IDbHelper _helper;

        public DbManager(ISqlFactory factory, IDbHelper helper)
        {
            _connection = factory;
            _helper = helper;
        }

        public IEnumerable<string> GetRaw(string selectQuery)
        {
            var connection = _connection.Create();
            var command = new SqlCommand(selectQuery, connection);
            var result = ExecuteQuery(command, connection);
            return result;
        }

        public DatabaseResponse ExecuteVoid(string commandText)
        {
            var connection = _connection.Create();
            var command = new SqlCommand(commandText, connection);
            var response = ExecuteVoid(command, connection);
            return response;
        }

        public IEnumerable<T> Get<T>(string selectQuery) where T : class
        {
            var connection = _connection.Create();
            var command = new SqlCommand(selectQuery, connection);
            var result = ExecuteQuery<T>(command, connection);
            return result;
        }

        private IEnumerable<T> ExecuteQuery<T>(SqlCommand command, SqlConnection connection)
        {
            var result = new List<T>();

            var rawData = ExecuteQuery(command, connection);
            foreach (var value in rawData)
            {
                var instance = (T)Activator.CreateInstance(typeof(T));

                var pairs = value
                    .Split('|')
                    .SelectMany(x => x.Split())
                    .Where(x => !string.IsNullOrEmpty(x));

                foreach (var pair in pairs)
                {
                    var pairValues = pair.Split(':');

                    var columnName = pairValues[0];
                    var columnValue = pairValues[1];
                    var dataType = pairValues[2];

                    var newValue = _helper.ParseValue(columnValue, dataType);

                    var prop = instance.GetType().GetProperty(columnName);
                    prop.SetValue(instance, newValue);
                }

                result.Add(instance);
            }

            return result;
        }

        private DatabaseResponse ExecuteVoid(SqlCommand command, SqlConnection connection)
        {
            var response = new DatabaseResponse();
            using (connection)
            {
                connection.Open();
                using (command)
                {
                    try
                    {
                        var affectedRows = command.ExecuteNonQuery();
                        response.AffectedRows = affectedRows;
                        response.IsSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        response.Exception = ex;
                    }
                }
            }

            return response;
        }

        private IEnumerable<string> ExecuteQuery(SqlCommand command, SqlConnection connection)
        {
            var result = new List<string>();
            using (connection)
            {
                connection.Open();
                using (command)
                {
                    try
                    {
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var value = new StringBuilder();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                var pair = _helper.GetColumnInfo(reader, i);
                                value.Append(pair);
                            }

                            result.Add(value.ToString());
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }
    }
}
