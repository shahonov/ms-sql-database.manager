﻿using System.Data.SqlClient;
using Database.Manager.Interfaces;

namespace Database.Manager.Implementations
{
    public class DbHelper : IDbHelper
    {
        public string GetColumnInfo(SqlDataReader reader, int index)
        {
            var columnName = reader.GetName(index);
            var columnValue = reader.GetValue(index);
            var sqlDataType = reader.GetDataTypeName(index);
            var columnDataType = MapToDotNetDataType(sqlDataType);

            var pair = string.Empty;
            if (index == reader.FieldCount - 1)
            {
                pair = $"{columnName}:{columnValue}:{columnDataType}";
            }
            else
            {
                pair = $"{columnName}:{columnValue}:{columnDataType} | ";
            }

            return pair;
        }

        public string MapToDotNetDataType(string sqlDataType)
        {
            switch (sqlDataType)
            {
                case "int":
                    return typeof(int).ToString().ToLower();

                case "float":
                    return typeof(double).ToString().ToLower();

                case "decimal":
                    return typeof(decimal).ToString().ToLower();

                case "bit":
                    return typeof(bool).ToString().ToLower();

                case "nvarchar":
                case "varchar":
                    return typeof(string).ToString().ToLower();

                default:
                    return null;
            }
        }

        public object ParseValue(string value, string dataType)
        {
            switch (dataType)
            {
                case "system.int32":
                    return int.Parse(value);

                case "system.boolean":
                    return bool.Parse(value);

                case "system.decimal":
                    return decimal.Parse(value);

                case "system.double":
                    return float.Parse(value);

                default:
                    return value;
            }
        }
    }
}
