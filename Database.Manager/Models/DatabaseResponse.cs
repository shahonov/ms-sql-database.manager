﻿using System;

namespace Database.Manager.Models
{
    public class DatabaseResponse
    {
        public DatabaseResponse(bool isSuccess = false, int affectedRows = 0, Exception ex = null)
        {
            IsSuccess = isSuccess;
            AffectedRows = affectedRows;
            Exception = ex;
        }

        public bool IsSuccess { get; set; }

        public int AffectedRows { get; set; }

        public Exception Exception { get; set; }

        public string Message => Exception?.Message;
    }
}
